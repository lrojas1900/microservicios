package com.family.relation.request;

import lombok.Data;

@Data
public class PersonaRelationRequest {
	String idPersonaPpal;
	String idPersonaSec;
	String parentesco;
}
