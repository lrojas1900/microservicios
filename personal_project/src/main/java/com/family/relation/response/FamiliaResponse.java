package com.family.relation.response;

import lombok.Data;

@Data
public class FamiliaResponse {
	DatosBasicosResponse  datosBasicosResponse;
	String parentesco;

}
