package com.family.relation.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.family.relation.entities.AuditoriaEntity;

@Repository
public interface AuditoriaRepository
    extends CrudRepository<AuditoriaEntity, Integer> {

}
