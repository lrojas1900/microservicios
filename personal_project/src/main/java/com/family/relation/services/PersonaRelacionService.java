package com.family.relation.services;

import static com.family.relation.utils.Constant.EVENTO_ELIMINAR;
import static com.family.relation.utils.Constant.RESP_EXITO;
import static com.family.relation.utils.Constant.RESP_FALLO;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.family.relation.adapter.PersonaAdapter;
import com.family.relation.entities.PersonaRelacionEntity;
import com.family.relation.repository.PersonaRelacionRepository;
import com.family.relation.request.PersonaRelationRequest;
import com.family.relation.utils.UtilCommons;

@Service
public class PersonaRelacionService {
  @Autowired
  PersonaRelacionRepository personaRelacionRepository;

  @Autowired
  PersonaAdapter personaAdapter;

  @Autowired
  AuditoriaService auditoriaService;

  public static String TABLA = "PersonaRelacion";
  
  
/**
 * savePersonaRelation Metodo usado para guardra la relacion de familiares de una persona
 * @param personaRelationRequest
 * @param metodo indica si será un operacion de guardar o actualizar
 * @return un boleano que indica si pudo o no realizar la operacion
 */
  public Boolean savePersonaRelation(PersonaRelationRequest personaRelationRequest, String metodo) {
    Gson gson = new Gson();
    String jsonPersonaRelationRequest = gson.toJson(personaRelationRequest);
    String valueOld = "";
    try {
      Optional<PersonaRelacionEntity> personaRelacionEntity =
          personaRelacionRepository.findByIdPersonaPpalAndSec(
              personaRelationRequest.getIdPersonaPpal(), personaRelationRequest.getIdPersonaSec());
      valueOld = personaRelacionEntity.isPresent() ? gson.toJson(personaRelacionEntity) : "";
      personaRelacionRepository
          .save(personaAdapter.getPersonaRelacionEntity(personaRelationRequest));
    } catch (Exception e) {
      auditoriaService.saveAuditoria(UtilCommons.createAuditoriaRequest(metodo, TABLA,
          "personaRelationRequest", jsonPersonaRelationRequest, valueOld, RESP_FALLO));
      return false;
    }
    auditoriaService.saveAuditoria(UtilCommons.createAuditoriaRequest(metodo, TABLA,
        "personaRelationRequest", jsonPersonaRelationRequest, valueOld, RESP_EXITO));
    return true;
  }
  
 /**
  * deletePersonaRelacion metodo para eliminar la relacion familiar de una persona
  * @param idPersonaPpal id de la persona del ppal del nucleo familiar
  * @param idPersonaSec id del familiar de la persona ppal
  * @return un boleano si pudo o no realizar la operacion
  */
  public Boolean deletePersonaRelacion(String idPersonaPpal,String idPersonaSec) {
    Gson gson = new Gson();
    String valueOld = "";
    try {
      Optional<PersonaRelacionEntity> personaRelacionEntity =
          personaRelacionRepository.findByIdPersonaPpalAndSec(
              idPersonaPpal, idPersonaSec);
      valueOld = personaRelacionEntity.isPresent() ? gson.toJson(personaRelacionEntity) : "";
      personaRelacionRepository.delete(personaRelacionEntity.get());
    } catch (Exception e) {
      auditoriaService.saveAuditoria(UtilCommons.createAuditoriaRequest(EVENTO_ELIMINAR, TABLA,
          idPersonaPpal + ","+idPersonaSec, "", valueOld, RESP_FALLO));
      return false;
    }
    auditoriaService.saveAuditoria(UtilCommons.createAuditoriaRequest(EVENTO_ELIMINAR, TABLA,
        idPersonaPpal + ","+idPersonaSec,"", valueOld, RESP_EXITO));
    return true;
  }


}
