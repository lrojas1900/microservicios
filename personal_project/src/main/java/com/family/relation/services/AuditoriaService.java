package com.family.relation.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.family.relation.adapter.AuditoriaAdapter;
import com.family.relation.repository.AuditoriaRepository;
import com.family.relation.request.AuditoriaRequest;

@Service
public class AuditoriaService {
  @Autowired
  AuditoriaRepository auditoriaRepository;
  
  @Autowired
  AuditoriaAdapter auditoriaAdapter;
  
  /**
   * saveAuditoria Metodo usado para guardar las trazas de auditoria
   * @param auditoriaRequest
   * @return retorna un boleano si pudo o no realizar la operacion
   */
  @Async
  public Boolean saveAuditoria(AuditoriaRequest auditoriaRequest) {
    try {
      auditoriaRepository.save(auditoriaAdapter.getAuditoriaEntity(auditoriaRequest));
    } catch (Exception e) {
      return false;
    }
    return true;
  }

}
